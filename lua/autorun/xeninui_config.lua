local function Load()
	-- Now you can edit XeninUI settings
	-- If you want to change the default blue color used for tabs etc. you can do this
	XeninUI.Theme.Accent = Color(206, 50, 50)
	-- A lot of people also want to disable the notifications
	XeninUI.DisableNotification = true
end

if (XeninUI) then
	Load()
else
	hook.Add("XeninUI.PostLoadSettings", "XeninUI.CustomConfig", Load)
end